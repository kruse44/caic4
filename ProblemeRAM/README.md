# cAIC4 RAM Probleme

## Untersuchungsgegenstand

Es gibt Probleme mit der RAM Auslastung bei der Nutzung des cAIC4.
Ein Lineares Mixed Model der folgenden Form wird gefittet:

```r
m1 <-lmer(log_labylm ~ educ + age + bilzeit + oexp + (1 | persnr),
          data = daten, REML = TRUE)
```
Nun werden die Befehle
```r
cAIC4::caic()  bzw.  cAIC4::anocAIC()
```
auf das Regressions-Objekt angewandt. Es kommt die folgenden Fehlermeldung:
```r
> cAIC(m010)
Error in .local(x, y, ...) : negative length vectors are not allowed
```

## Analyse der Codestruktur

Durch manuelles laden der Dev-Codebasis und einem trace-back des Problems ergibt sich das folgende Bild:
```r
 Error in .local(x, y, ...) : negative length vectors are not allowed 

8. .local(x, y, ...) 
7. crossprod(crossprod(X %*% solve(getME(m, "RX")), V0inv)) 
6. crossprod(crossprod(X %*% solve(getME(m, "RX")), V0inv)) at getModelComponents.R#123
5. getModelComponents.merMod(zeroLessModel, analytic) at getModelComponents.R#1
4. getModelComponents(zeroLessModel, analytic) at biasCorrectionGaussian.R#20
3. biasCorrectionGaussian(object, sigma.penalty, analytic) at bcMer.R#28
2. bcMer(object, method = method, B = B, sigma.penalty = sigma.penalty, 
    analytic = analytic) at cAIC.R#248
1. cAIC(m010) 
```

## Analyse der Fehler-Meldung
```
Error in .local(x, y, ...) : negative length vectors are not allowed 
```

Dieser Fehler ist typisch für Probleme mit (Data.Frame)-Memory-Limits. Um genauer zu sein handelt es sich hierbei um Memory-Corruption welche dadurch entsteht, dass man über den allokierten Memory hinaus für die Spalte und/oder Zeile versucht zu schreiben.

Quellen:
- https://stackoverflow.com/questions/36842263 memory-limits-in-data-table-negative-length-vectors-are-not-allowed
- https://stackoverflow.com/questions/16877027/negative-number-of-rows-in-data-table-after-incorrect-use-of-set
- https://stat.ethz.ch/pipermail/r-help/2015-January/425051.html


## Welcher Teil des Code verursacht diesen Fehler? 

Durch den Traceback wissen wir, dass der Fehler in getModelComponents.merMod zu finden ist. An der Stelle ll.48
```r
A   <- V0inv - crossprod(crossprod(X %*% solve(getME(m, "RX")), V0inv))
```

Hierbei wird schnell ersichtlich, dass das Problem durch den aufruf des doppelten Crossprod entsteht. Umgenauer zu sein, tritt das Problem erst bei der zweiten Anwendung des Crossproduct Operators auf sprich:
```r
foo <- crossprod(X %*% solve(getME(m, "RX")), V0inv)
crossprod(foo)
Error in .local(x, y, ...) : negative length vectors are not allowed 
```

## Mögliche Gründe

- <del>Nutzen von sparsen Matrizen führt zu Problemen mit crossprod-Befehl, da jetzt R den Matrix-Package Befehl nimmt und nicht dem aus base-R.
- <del>fehler in der Reihenfolge der argumente im Cross-Prod.</del>
- <del>Problem durch neue V0inv durch sparse Berechnung
- <b>Pech? ... quasi!</b> 

## Analyse woran es liegt

Interessanterweise funktioniert der Call bei der Berechnung einfacher Modell wie des im lme4 gegebenen sleepstudy Beispieles

```r
cAIC(lmer(Reaction ~ Days + (Days | Subject), sleepstudy))
                                                   
               Conditional log-likelihood:  -824.51
                       Degrees of freedom:    31.30
 Conditional Akaike information criterion:  1711.62

```
Ebenso funktionert die Anwendung der Funktion auf den kleineren Datensatz der auf das Jahr 2010 sich bezieht.
```r
m010 <- lmer(log_labylm ~ educ + age + bilzeit + oexp + (1 | bula), 
             data = daten2000, REML = TRUE)

cAIC(m010)
                                                    
               Conditional log-likelihood:  -9631.33
                       Degrees of freedom:     20.31
 Conditional Akaike information criterion:  19303.27
```

Die Schlussfolgerung ist, dass die Analyse der Fehlermeldung recht hat und die größe der des Objekts zu groß ist um die Berchnung durch zu führen. Jetzt sollte man dieses Problem beheben können, die frage ist nur wie.

# Lösungsvorschläge

Man könnte die Größe minimieren, in dem man die Berechnung des Kreuzproduktes Schritt-für-Schritt erledigen könnten. Vielleicht könnte man es relativ einfach machen über die Apply-familie oder ähnliche Anwendungen.

# Libraries:
library(cAIC4ModelAveraging)
library(ggplot2)
library(hexbin)
library(lme4)
# daten einlesen:
Datensatz_7_10 <- read.csv("~/ownCloud/projekte/2019/Model Averaging Anwendung/daten/Datensatz_7_10.csv")
daten <- Datensatz_7_10[Datensatz_7_10$labylM>0,]
# NAs raus
daten <- na.omit(daten)
# erfahrungsvariable erstellen:
daten$oexp <- daten$expft + daten$exppt 
summary(daten)

# Datenbereinigen:
# - health (negativ?)
# - bilzeit (negativ?)
# - isced (negativ?)
# - erfahrung (negativ?)
# - income (negativ?)
# Negative bildungszeiten
daten <- daten[daten$bilzeit>0, ]
# Negativer Abschluss
daten <- daten[daten$isced>=0, ]
# Negative Erfahrung
daten <- daten[daten$expft>=0, ]
daten <- daten[daten$exppt>=0, ]
# Negative Einkommen
daten <- daten[daten$income>0, ]
summary(daten)

# daten anpassen:
# log of monthly labour income
daten$log_labylm <- log(daten$labylM)
# binary dummy for higher education (isced 5 + 6)
daten$educ <- NA
daten$educ[daten$isced %in% c(5,6)] <- 1
daten$educ[daten$isced %in% c(1,2,3,4)] <- 0
# Nur für ein Jahr:
daten2000 <- daten[daten$year==2000, ] 

saveRDS(daten, file = "daten.RDS")
saveRDS(daten2000, file = "daten2000.RDS")
